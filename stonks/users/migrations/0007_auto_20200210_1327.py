# Generated by Django 2.2 on 2020-02-10 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20200210_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='rub',
            field=models.BooleanField(default=True),
        ),
    ]
